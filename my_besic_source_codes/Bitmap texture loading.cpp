#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/gl.h>
#include<gl/glu.h>

float angle = 0.0;
unsigned int loadTexture(const char* filename)
{
    SDL_Surface* img=SDL_LoadBMP(filename);
    unsigned int id;
    glGenTextures(1,&id);
    glBindTexture(GL_TEXTURE_2D,id);
    glTexImage2D(GL_TEXTURE_2D,0,GL_RGB,img->w,img->h,0,GL_RGB,GL_UNSIGNED_SHORT_5_6_5,img->pixels);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    SDL_FreeSurface(img);
    return id;
}
void simplequad(float size)
{
    glBegin(GL_QUADS);
    glColor3f(1.0,1.0,1.0);
    glTexCoord2f(0.0,1.0);
    glVertex3f(-size/2,size/2,0.0);
    glTexCoord2f(0.0,0.0);
    glVertex3f(-size/2,-size/2,0.0);
    glTexCoord2f(1.0,0.0);
    glVertex3f(size/2,-size/2,0.0);
    glTexCoord2f(1.0,1.0);
    glVertex3f(size/2,size/2,0.0);
    glEnd();

}

unsigned int tex;
void init()
{
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,640.0/480.0,1.0,500.0);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    tex=loadTexture("Brick.bmp"); //Texture has loaded
}

    void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0,0.0,-5.0);
    glRotatef(angle,1.0,1.0,1.0);
    glBindTexture(GL_TEXTURE_2D,tex);
    simplequad(4.0);
 }

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);
    printf("OpenGL is running..");

    //DEFAULT BEST MEMORY ALLOCATION
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

    SDL_WM_SetCaption("Game Window",NULL);  //SET WINDOW CAPTION

    SDL_SetVideoMode(640,480,32,SDL_OPENGL);  //SET WINDOW SIZE

    glViewport(0,0,640,480);  //SET CO-ORDINATE POSITION(0,0) AND (600,400)

    //handles the main loop
     bool isRunning =true;

    //for handling with event
    SDL_Event event;
	init();
    while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }

        }

        display();
        SDL_GL_SwapBuffers();
        angle += 0.02;
        if(angle>360)
        angle-=360;
    }

    SDL_Quit();
    return 0;

}

#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/gl.h>
#include<gl/glu.h>


void init()
{
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,640.0/480.0,1.0,00.0);
    glMatrixMode(GL_MODELVIEW);
}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0,0.0,-7.0);
    glRotatef(85,0.0,2.0,0.0);
    glBegin(GL_TRIANGLES);
    glColor4f(1.0,0.0,0.0,1.0);
    glVertex3f(0.0,2.0,-0.0);
    glVertex3f(-2.0,-2.0,-0.0);
    glVertex3f(2.0,-2.0,-0.0);
    glEnd();
}

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);

    //DEFAULT BEST MEMORY ALLOCATION
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

    SDL_WM_SetCaption("My Window",NULL);  //SET WINDOW CAPTION

    SDL_SetVideoMode(640,480,32,SDL_OPENGL);  //SET WINDOW SIZE

    glViewport(0,0,640,480);  //SET CO-ORDINATE POSITION(0,0) AND (600,400)

    //Main game loop

    //handles the main loop
     bool isRunning = true;

    //for handling with event
    SDL_Event event;

    init();
    while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }

        }

        display();
        //SDL_Delay();
        SDL_GL_SwapBuffers();
    }

    SDL_Quit();
    return 0;

}

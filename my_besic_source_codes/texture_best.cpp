#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/glu.h>


float angle=0.0;
GLuint tex;
GLuint loadTexture(const char* filename)
{
    SDL_Surface* img=SDL_LoadBMP(filename);
    SDL_Surface* optimized=SDL_DisplayFormat(img);
    SDL_FreeSurface(img);
    GLuint id;
    glGenTextures(1,&id);
    glBindTexture(GL_TEXTURE_2D,id);
    gluBuild2DMipmaps(GL_TEXTURE_2D,GL_RGBA32F_ARB,img->w,img->h,GL_BGR_EXT,GL_UNSIGNED_BYTE,img->pixels);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    SDL_FreeSurface(optimized);
    return id;
}

void simplequad(float size)
{
    glBegin(GL_QUADS);
    glColor3f(1.0,1.0,1.0);
    glTexCoord2f(0.0,1.0);
    glVertex3f(-size/2,size/2,0.0);
    glTexCoord2f(0.0,0.0);
    glVertex3f(-size/2,-size/2,0.0);
    glTexCoord2f(1.0,0.0);
    glVertex3f(size/2,-size/2,0.0);
    glTexCoord2f(1.0,1.0);
    glVertex3f(size/2,size/2,0.0);
    glEnd();
}

void init()
{
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60,640.0/480.0,1.0,500.0);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_TEXTURE_2D);
    tex=loadTexture("pic.bmp");

}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0,0.0,-5.0);
    glRotatef(angle,1.0,0.0,0.0);
    glBindTexture(GL_TEXTURE_2D,tex);
    simplequad(3.0);
}

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);

    //DEFAULT BEST MEMORY ALLOCATION
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

    SDL_WM_SetCaption("My Window",NULL);  //SET WINDOW CAPTION

    SDL_SetVideoMode(640,480,32,SDL_OPENGL);  //SET WINDOW SIZE

    glViewport(0,0,640,480);  //SET CO-ORDINATE POSITION(0,0) AND (600,400)

    //Main game loop

    //handles the main loop
     bool isRunning = true;

    //for handling with event
    SDL_Event event;

    init();
    while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }

        }

        display();
        if(angle>360)
        {
            angle-=360;
        }
        else
        angle+=0.1;
        //SDL_Delay();
        SDL_GL_SwapBuffers();
    }

    SDL_Quit();
    return 0;

}

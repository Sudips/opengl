#ifndef TEX_H_INCLUDED
#define TEX_H_INCLUDED
#include<sdl/sdl.h>
#include<sdl/sdl_opengl.h>

GLuint loadTexture(const char* filename);
void BindImg(float,float,float,float);

#endif // TEX_H_INCLUDED

#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/glu.h>

float angle = 0.0;
void drawcube(float size)
{
    GLfloat difamb[]={1.0,0.0,0.0,1.0};
    glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,difamb);
    //glMateriali(GL_FRONT,GL_SHININESS,128);
    glBegin(GL_QUADS);
    //Front face
    glNormal3f(0.0,0.0,1.0);
    glVertex3f(size/2,size/2,size/2);
    glVertex3f(-size/2,size/2,size/2);
    glVertex3f(-size/2,-size/2,size/2);
    glVertex3f(size/2,-size/2,size/2);

    //Left face
    glNormal3f(-1.0,0.0,0.0);
    glVertex3f(-size/2,size/2,size/2);
    glVertex3f(-size/2,-size/2,size/2);
    glVertex3f(-size/2,-size/2,-size/2);
    glVertex3f(-size/2,size/2,-size/2);

    //Back face
    glNormal3f(0.0,0.0,-1.0);
    glVertex3f(size/2,size/2,-size/2);
    glVertex3f(-size/2,size/2,-size/2);
    glVertex3f(-size/2,-size/2,-size/2);
    glVertex3f(size/2,-size/2,-size/2);

    //Right face
    glNormal3f(1.0,0.0,0.0);
    glVertex3f(size/2,size/2,size/2);
    glVertex3f(size/2,-size/2,size/2);
    glVertex3f(size/2,-size/2,-size/2);
    glVertex3f(size/2,size/2,-size/2);

    //Top face
    glNormal3f(0.0,1.0,0.0);
    glVertex3f(size/2,size/2,size/2);
    glVertex3f(-size/2,size/2,size/2);
    glVertex3f(-size/2,size/2,-size/2);
    glVertex3f(size/2,size/2,-size/2);

    //Bottom face
    glNormal3f(0.0,1.0,0.0);
    glVertex3f(size/2,-size/2,size/2);
    glVertex3f(-size/2,-size/2,size/2);
    glVertex3f(-size/2,-size/2,-size/2);
    glVertex3f(size/2,-size/2,-size/2);

    glEnd();
}


void init()
{
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,640.0/480.0,1.0,500.0);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    //glEnable(GL_COLOR_MATERIAL);
    GLfloat ambient[]={0.2,0.2,0.2,1.0};
    glLightfv(GL_LIGHT0,GL_AMBIENT,ambient);
    GLfloat diffuse[]={1.0,1.0,1.0,1.0};
    glLightfv(GL_LIGHT0,GL_DIFFUSE,diffuse);

}

    void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    GLfloat position[]={-2.0,2.0,-3.0,1.0};
    glLightfv(GL_LIGHT0,GL_POSITION,position);
    glTranslatef(0.0,0.0,-5.0);
    glRotatef(angle,1.0,1.0,1.0);
    drawcube(1.0);

}

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);
    printf("OpenGL is running..");

    //DEFAULT BEST MEMORY ALLOCATION
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

    SDL_WM_SetCaption("Game Window",NULL);  //SET WINDOW CAPTION

    SDL_SetVideoMode(600,400,32,SDL_OPENGL);  //SET WINDOW SIZE

    glViewport(0,0,600,400);  //SET CO-ORDINATE POSITION(0,0) AND (600,400)

    glMatrixMode(GL_PROJECTION);  //For 2D rendering
    glLoadIdentity();   //For saving rendered 2D image

    //Main game loop

    //handles the main loop
     int isRunning=1;

    //for handling with event
    SDL_Event event;
	init();
    while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }

        }

        display();
        SDL_GL_SwapBuffers();
        angle += 0.1;
        if(angle>360)
        angle-=360;
        SDL_Delay(1);
    }

    SDL_Quit();
    return 0;

}

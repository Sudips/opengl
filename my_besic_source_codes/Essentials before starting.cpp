#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/gl.h>
#include<gl/glu.h>

float angle = 0.0;
void drawcube(float size)
{
    //float difamb[]={1.0,0.0,0.0,1.0};
    glBegin(GL_QUADS);
    //Front face
    glNormal3f(0.0,0.0,1.0);
    //glLightfv(GL_LIGHT0,GL_SPECULAR,difamb);
    glVertex3f(size/2,size/2,size/2);
    glVertex3f(-size/2,size/2,size/2);
    glVertex3f(-size/2,-size/2,size/2);
    glVertex3f(size/2,-size/2,size/2);

    //Left face
    glNormal3f(-1.0,0.0,0.0);

    glVertex3f(-size/2,size/2,size/2);
    glVertex3f(-size/2,-size/2,size/2);
    glVertex3f(-size/2,-size/2,-size/2);
    glVertex3f(-size/2,size/2,-size/2);

    //Back face
    glNormal3f(0.0,0.0,1.0);

    glVertex3f(size/2,size/2,-size/2);
    glVertex3f(-size/2,size/2,-size/2);
    glVertex3f(-size/2,-size/2,-size/2);
    glVertex3f(size/2,-size/2,-size/2);

    //Right face
    glNormal3f(1.0,0.0,0.0);

    glVertex3f(size/2,size/2,size/2);
    glVertex3f(size/2,-size/2,size/2);
    glVertex3f(size/2,-size/2,-size/2);
    glVertex3f(size/2,size/2,-size/2);

    //Top face
    glNormal3f(0.0,1.0,0.0);
    glVertex3f(size/2,size/2,size/2);
    glVertex3f(-size/2,size/2,size/2);
    glVertex3f(-size/2,size/2,-size/2);
    glVertex3f(size/2,size/2,-size/2);

    //Bottom face
    glNormal3f(0.0,-1.0,0.0);

    glVertex3f(size/2,-size/2,size/2);
    glVertex3f(-size/2,-size/2,size/2);
    glVertex3f(-size/2,-size/2,-size/2);
    glVertex3f(size/2,-size/2,-size/2);

    glEnd();
}
void init()
{
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(45,640.0/480.0,1.0,500.0);
    glMatrixMode(GL_MODELVIEW);
    glEnable(GL_DEPTH_TEST);
    // Somewhere in the initialization part of your program� �
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    // Create light components
    float diffuseLight[] = { 0.8f, 0.8f, 0.8, 1.0f };
    // Assign created components to GL_LIGHT0
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight);
     //Material properties set
}

    void display()
{
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();
    //float pos[]={-10.0,10.0,-3.0,1.0};
    //glLightfv(GL_LIGHT0,GL_POSITION,pos);
    glTranslatef(0.0,0.0,-5.0);
    glRotatef(angle,1.0,1.0,1.0);
    drawcube(1.0);

}

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);
    printf("OpenGL is running..");

    //DEFAULT BEST MEMORY ALLOCATION
    SDL_GL_SetAttribute(SDL_GL_RED_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE,8);
    SDL_GL_SetAttribute(SDL_GL_BUFFER_SIZE,32);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER,1);

    SDL_WM_SetCaption("Game Window",NULL);  //SET WINDOW CAPTION

    SDL_SetVideoMode(600,400,32,SDL_OPENGL);  //SET WINDOW SIZE

    glViewport(0,0,600,400);  //SET CO-ORDINATE POSITION(0,0) AND (600,400)

    glMatrixMode(GL_PROJECTION);  //For 2D rendering
    glLoadIdentity();   //For saving rendered 2D image

    //Main game loop

    //handles the main loop
     bool isRunning =true;

    //for handling with event
    SDL_Event event;
	init();
    while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }

        }

        display();
        glBegin(GL_POINT);
        glColor3f(1.0,1.0,1.0);
        glVertex3f(-2.0,2.0,-3.0);
        glEnd();
        SDL_GL_SwapBuffers();
        angle += 0.02;
        if(angle>360)
        angle-=360;
    }

    SDL_Quit();
    return 0;

}

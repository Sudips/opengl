#include<sdl/sdl.h>
#include<sdl_opengl.h>
#include<gl/glu.h>
#include<box2d/box2d.h>

class FooTest : public Test
{
   public:
   FooTest() { } //do nothing, no scene yet
   void Step(Settings* settings)
   {
      //run the default physics and rendering
      Test::Step(settings);
      //show some text in the main screen
      m_debugDraw.DrawString(5, m_textLine, "Now we have a foo test");
      m_textLine += 15;
   }
   static Test* Create()
   {
      return new FooTest;
   }
};

void init()
{
    glClearColor(0.0,0.0,0.0,1.0);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60,640.0/480.0,1.0,500.0);
    glDisable(GL_DEPTH_TEST);

}
void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    glTranslatef(0.0,0.0,-5.0);
    //glRotatef(angle,1.0,0.0,0.0);
}

int main(int argc, char* argv[])
{
    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_WM_SetCaption("My Window",NULL);
    SDL_SetVideoMode(640,480,32,SDL_OPENGL);
    glViewport(0,0,640,480);
    bool isRunning = true;
    SDL_Event event;
    init();
    while(isRunning)
    {
        //input or enevts
        while(SDL_PollEvent(&event))
        {
            if(event.type==SDL_QUIT)
            {
                isRunning = false;
            }
            if(event.type==SDL_KEYDOWN && event.key.keysym.sym==SDLK_ESCAPE)
            {
                isRunning = false;
            }
        }

        display();
        SDL_GL_SwapBuffers();
    }

    SDL_Quit();
    return 0;

}
